Solve a problem anyway.

There is a resource with some startups info "https://www.eu-startups.com/directory/".

The problem is to collect info about companies (location, description et c.), plus computing their location coordinates.

I wish to solve this problem in order to train myself (in that technology fileld). Or to examine which hidden complexity there may be found.

Technologies that seems to suit well is Python/Scrapy/geopy.

2021-03-06

Detailed problem.

There is a list of a countries at the start page. I need Scrapy to crawl each country and each startup within each country.

First and probably most valuable result is complete - you can find results of crawl in folder "results". There is a startups' names.

2021-03-08 : needed data is collected
