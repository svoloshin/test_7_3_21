import scrapy


class EuStartupsSpider(scrapy.Spider):
    name = 'eu_startups'
    allowed_domains = ['eu-startups.com']
    # start_urls = ['http://eu-startups.com/']

    def start_requests(self):
      urls = [
        'https://www.eu-startups.com/directory/'
      ]

      for url in urls:
        yield scrapy.Request(url=url, callback=self.parse)


    def is_start_page(self, response):
      header_a = response.css("div.td-page-header")
      header_b = header_a.css("h1.entry-title.td-page-title")
      header = header_b.css("span::text").get()

      if (header == 'Startup Database'):
        self.log(f'Found initial page.')
        return True

      return False

    def is_a_country_name_page(self, response):
      if (response.css("h2.category-name") != []):
        self.log(f'Found country name page.')
        return True
      return False


    def is_a_startup_page(self, response):

      company_title = response.css("div.listing-title").css("h2::text").get()
      if (company_title and len(company_title) > 0):
        return True
        self.log(f'Found a startup page')

      return False

    def there_is_a_next_country_startups_list_page(self, response):
      next_link = response.css("div.wpbdp-pagination").css("span.next").css("a::attr(href)").get()
      if (next_link and len(next_link)>0):
        self.log(f'Found a next link.')
        return True
      return False


    def parse(self, response):
      page = response.url.split("/")[-2]
      filename = f'results/eu_startups-{page}.txt'

      # possibilities for a page that is examined:
      # it is a start page - then obtain country_name_pages
      # it is a country name page - then obtain pages of a country startups
      # it is a country startup page - then obtain needed info and write it

      if self.is_start_page(response):
        country_name_pages = response.css("li.cat-item a::attr(href)")

        for country_name_page in country_name_pages:
          next_page = country_name_page.get()
          yield scrapy.Request(next_page, callback=self.parse)

      if self.is_a_country_name_page(response):

        country_startups_list = response.css("div.listings.wpbdp-listings-list.list")

        for country_startup in country_startups_list:
          next_page = country_startup.css("div.listing-title").css("a::attr(href)").get()
          yield scrapy.Request(next_page, callback=self.parse)

        if self.there_is_a_next_country_startups_list_page(response):
          next_link = response.css("div.wpbdp-pagination").css("span.next").css("a::attr(href)").get()
          next_page = next_link
          yield scrapy.Request(next_page, callback=self.parse)

      if self.is_a_startup_page(response):

        # Business Name
        # Category
        # Business Description
        # Long Business Description
        # Based in
        # Tags
        # Total Funding
        # Founded
        # Website

        business_name = response.css("div.wpbdp-field-display.wpbdp-field.wpbdp-field-value.field-display.field-value.wpbdp-field-business_name.wpbdp-field-title.wpbdp-field-type-textfield.wpbdp-field-association-title").css("span.value").css("a::text").get()

        category = response.css("div.wpbdp-field-display.wpbdp-field.wpbdp-field-value.field-display.field-value.wpbdp-field-category.wpbdp-field-category.wpbdp-field-type-select.wpbdp-field-association-category").css("span.value").css("a::text").get()

        business_description = response.css("div.wpbdp-field-display.wpbdp-field.wpbdp-field-value.field-display.field-value.wpbdp-field-business_description.wpbdp-field-meta.wpbdp-field-type-textarea.wpbdp-field-association-meta").css("span.value::text").get()

        long_business_description = ''
        lbc_paragraphs = response.css("div.wpbdp-field-display.wpbdp-field.wpbdp-field-value.field-display.field-value.wpbdp-field-long_business_description.wpbdp-field-content.wpbdp-field-type-textarea.wpbdp-field-association-content").css("span.value").css("p::text")
        for paragraph in lbc_paragraphs:
          long_business_description = long_business_description + paragraph.get() + '\n'

        based_in = response.css("div.wpbdp-field-display.wpbdp-field.wpbdp-field-value.field-display.field-value.wpbdp-field-based_in.wpbdp-field-meta.wpbdp-field-type-textfield.wpbdp-field-association-meta").css("span.value::text").get()

        tags = response.css("div.wpbdp-field-display.wpbdp-field.wpbdp-field-value.field-display.field-value.wpbdp-field-tags.wpbdp-field-meta.wpbdp-field-type-textfield.wpbdp-field-association-meta").css("span.value::text").get()

        total_funding = response.css("div.wpbdp-field-display.wpbdp-field.wpbdp-field-value.field-display.field-value.wpbdp-field-total_funding.wpbdp-field-meta.wpbdp-field-type-select.wpbdp-field-association-meta").css("span.value::text").get()

        founded = response.css("div.wpbdp-field-display.wpbdp-field.wpbdp-field-value.field-display.field-value.wpbdp-field-founded.wpbdp-field-meta.wpbdp-field-type-select.wpbdp-field-association-meta").css("span.value::text").get()

        website = response.css("div.wpbdp-field-display.wpbdp-field.wpbdp-field-value.field-display.field-value.wpbdp-field-website.wpbdp-field-meta.wpbdp-field-type-textfield.wpbdp-field-association-meta").css("span.value::text").get()

        # with open(filename, 'wb') as f:
        with open(filename, 'w') as f:

          startup_info = "{0}\n{1}\n{2}\n{3}\n{4}\n{5}\n{6}\n{7}\n{8}".format(
            business_name,
            category,
            business_description,
            long_business_description,
            based_in,
            tags,
            total_funding,
            founded,
            website
          )

          f.write(startup_info)
          self.log(f'Saved file {filename}')
